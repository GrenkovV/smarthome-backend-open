var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser')
var cors = require('cors')
var morgan = require('morgan')
let ngrok = require('ngrok');
let firebase = require('./firebase');
let auth = require('./auth');
let smarthome = require('./smarthome');
let shcedule = require('./schedule');

var fs = require('fs');
var http = require('http');
var https = require('https');
const config = require('./config')

let sslCredentials = {
   cert: fs.readFileSync('../certs/victorylight-smart.com/fullchain.pem'),
   key: fs.readFileSync('../certs/victorylight-smart.com/privkey.pem')
};

var app = express();


// Create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: true })
app.use(express.static('public'));
app.use(cookieParser())
app.set("view engine", "hbs");

app.use(cors())
app.use(morgan('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))


app.post('/test', urlencodedParser, function (req, res) {

   res.send('done');
});


app.get('/', auth.login_get)
app.get('/login', auth.login_get)

app.post('/doLogin', urlencodedParser, auth.login_post)

app.use('/link', urlencodedParser, auth.checkAuthMiddleware);
app.use('/seed', urlencodedParser, auth.checkAuthMiddleware);

app.post('/link', urlencodedParser, auth.link);
app.get('/auth', urlencodedParser, auth.auth);
app.all('/token', urlencodedParser, auth.token);

// app.use('/smarthome', urlencodedParser, auth.checkAccessTokenMiddleware);
app.post('/smarthome', smarthome.smarthomeApp);

app.post('/seed', smarthome.devicesHelper.seedDevices);

// Load Balancer ping request.
app.get('/ping',  (req, res) => {
   res.status(200).send('pong');
});


var httpServer = http.createServer(app);
var httpsServer = https.createServer(sslCredentials, app);

httpServer.listen(config.expressPort, function () {
   var host = this.address().address
   var port = this.address().port

   console.log("HTTP app listening at http://%s:%s", host, port)

   initNgrok();
});

httpsServer.listen(config.expressPortSsl, function () {
   var host = this.address().address
   var port = this.address().port

   console.log("HTTPS app listening at http://%s:%s", host, port)
})

async function initNgrok() {

   if (config.useNgrok) {
      try {
         const url = await ngrok.connect(config.expressPort)
         console.log('Replace the webhook URL in the Actions section with:')
         console.log('    ' + url + '/smarthome')

         console.log('')
         console.log('In the console, set the Authorization URL to:')
         console.log('    ' + url + '/auth')

         console.log('')
         console.log('Then set the Token URL to:')
         console.log('    ' + url + '/token')
         console.log('')

         console.log('Finally press the \'TEST DRAFT\' button')
      } catch (err) {
         console.error('Ngrok was unable to start')
         console.error(err)
         process.exit()
      }
   }
}
