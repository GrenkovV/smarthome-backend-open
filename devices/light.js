const { Device } = require('./device');

class Light extends Device {

    defaultDevice() {
        return {
            id: this.randomId(),
            type: 'action.devices.types.LIGHT',
            traits: [
                'action.devices.traits.OnOff',
                'action.devices.traits.Brightness',
                'action.devices.traits.ColorSetting'
            ],
            name: {
                defaultNames: ['Default Color Lamp L1'],
                name: 'lamp1',
                nicknames: ['reading lamp']
            },
            willReportState: false,
            attributes: {
                colorModel: 'rgb',
                colorTemperatureRange: {
                    temperatureMinK: 2000,
                    temperatureMaxK: 6500
                }
            },
            deviceInfo: {
                manufacturer: 'GrenSoft',
                model: 'model-l1',
                hwVersion: '1.0',
                swVersion: '1.0'
            },
            customData: {
                fooValue: 12,
                barValue: false,
                bazValue: 'dancing alpaca'
            },
            states: {
                on: false,
                online: false,
                brightness: 0,
                color: {
                    spectrumRgb: 0
                },
                wasOnline: 0,
            }
        };
    }

}

module.exports = {
    Light,
}