const { Device } = require('./device');

class Outlet extends Device {

    defaultDevice() {
        return {
            id: this.randomId(),
            type: 'action.devices.types.OUTLET',
            traits: [
                'action.devices.traits.OnOff'
            ],
            name: {
                defaultNames: ['Outlet M001'],
                name: 'Vitaly Simple Outlet',
                nicknames: ['wall plug']
            },
            willReportState: false,
            deviceInfo: {
                manufacturer: 'GrenSoft',
                model: 'model-o1',
                hwVersion: '1.0',
                swVersion: '1.0'
            },
            customData: {
                fooValue: 74,
                barValue: true,
                bazValue: 'sheepdip'
            },
            states: {
                on: false,
                online: false,
                wasOnline: 0,
            }
        };
    }

}

module.exports = {
    Outlet,
}