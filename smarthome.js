// Smart home imports
const firebase = require('./firebase');
const { smarthome } = require('actions-on-google');
const { Outlet } = require('./devices/outlet');
const { Light } = require('./devices/light');

let jwt = require('./keys/smart-home-key.json');
const { firestore } = require('firebase-admin');

const smarthomeApp = smarthome({
    jwt,
    debug: true,
});

firebase.functions.setSmartHomeApp(smarthomeApp);

smarthomeApp.onSync(async (body, headers) => {

    let token = headers.authorization.substring(7);
    let user = await firebase.functions.getUserByAccessToken(token)

    let devices = await firebase.functions.getDevices(user.id);
    for(let i=0; i<devices.length; i++) {
        delete devices[i].states;
    }

    // TODO Get devices for user
    return {
        requestId: body.requestId,
        payload: {
            agentUserId: user.id,
            devices: devices
        }
    };
});

smarthomeApp.onQuery(async (body, headers) => {

    let token = headers.authorization.substring(7);
    let user = await firebase.functions.getUserByAccessToken(token)

    let deviceStates = {};

    const { devices } = body.inputs[0].payload

    for (let i = 0; i < devices.length; i++) {
        let deviceId = devices[i].id;
        let device = await firebase.functions.getDevice(user.id, deviceId);

        if (device.states) {
            deviceStates[deviceId] = Object.assign({ status: 'SUCCESS' }, device.states);
        }
    }

    return {
        requestId: body.requestId,
        payload: {
            devices: deviceStates,
        },
    }
})


smarthomeApp.onExecute(async (body, headers) => {

    let token = headers.authorization.substring(7);
    let user = await firebase.functions.getUserByAccessToken(token)

    const userId = user.id;
    const successCommand = {
        ids: [],
        status: 'SUCCESS',
        states: {},
    }
    let commands = [];

    const { devices, execution } = body.inputs[0].payload.commands[0]
    for (let i = 0; i < devices.length; i++) {
        let device = devices[i];
        try {
            const states = await firebase.functions.execute(userId, device.id, execution[0])
            successCommand.ids.push(device.id)
            successCommand.states = states
            await firebase.functions.reportState(userId, device.id, states)
        } catch (e) {
            console.error(e)
            if (e.message === 'pinNeeded') {
                commands.push({
                    ids: [device.id],
                    status: 'ERROR',
                    errorCode: 'challengeNeeded',
                    challengeNeeded: {
                        type: 'pinNeeded',
                    },
                })
            } else if (e.message === 'challengeFailedPinNeeded') {
                commands.push({
                    ids: [device.id],
                    status: 'ERROR',
                    errorCode: 'challengeNeeded',
                    challengeNeeded: {
                        type: 'challengeFailedPinNeeded',
                    },
                })
            } else if (e.message === 'ackNeeded') {
                commands.push({
                    ids: [device.id],
                    status: 'ERROR',
                    errorCode: 'challengeNeeded',
                    challengeNeeded: {
                        type: 'ackNeeded',
                    },
                })
            } else if (e.message === 'PENDING') {
                commands.push({
                    ids: [device.id],
                    status: 'PENDING',
                })
            } else {
                commands.push({
                    ids: [device.id],
                    status: 'ERROR',
                    errorCode: e.message,
                })
            }
        }
    }

    if (successCommand.ids.length) {
        commands.push(successCommand)
    }

    return {
        requestId: body.requestId,
        payload: {
            commands,
        },
    }
})
  
smarthomeApp.onDisconnect(async (body, headers) => {

    let token = headers.authorization.substring(7);
    let user = await firebase.functions.getUserByAccessToken(token);

    await firebase.functions.setHomegraphEnable(user.id, false);
})
  

class DevicesHelper {

    seedDevices(req, res) {

        // let demoOutlet = new Outlet().defaultDevice();
        let demoLight = new Light().defaultDevice();

        demoLight.willReportState = true;
        demoLight.id = 'max01';
        demoLight.states.online = true;

        // firebase.functions.addDevice(req.custom.userId, demoOutlet);
        firebase.functions.addDevice(req.custom.userId, demoLight);
    
        res.status(200).send('Device Seeded');
    }

}

const devicesHelperObject = new DevicesHelper();



module.exports = {
    // fulfillment: smarthomeObject.fulfillment,
    smarthomeApp: smarthomeApp,
    devicesHelper: devicesHelperObject,
    functions: {
    },
}