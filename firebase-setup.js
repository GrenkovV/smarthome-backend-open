// Copy and paste this into your JavaScript code to initialize the Firebase SDK.
// You will also need to load the Firebase SDK.
// See https://firebase.google.com/docs/web/setup for more details.

firebase.initializeApp({
  "apiKey": "",
  "databaseURL": "https://victory-light.firebaseio.com",
  "storageBucket": "victory-light.appspot.com",
  "authDomain": "victory-light.firebaseapp.com",
  "messagingSenderId": "",
  "projectId": "t"
});

