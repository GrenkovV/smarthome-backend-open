let firebase = require('./firebase');
let uuid = require('uuid');
let util = require('util');

class AuthClass {

  login_post(req, res) {
    // Prepare output in JSON format
    response = {
      first_name: req.body.first_name,
      last_name: req.body.last_name
    };
    res.end(JSON.stringify(response));
  }

  login_get(req, res) {

    res.render('login.hbs', { responseurl: req.query.responseurl, code: req.query.code });
  }

  checkAuthMiddleware(req, res, next) {
    let token = req.headers.authToken || req.body.authToken;
    if (token) {
      firebase.functions.checkAuth(token)
        .then((decodedId) => {
          req.custom = {
            'userId': decodedId.uid,
          };
          next()
        }).catch(() => {
          res.status(403).send('Unauthorized')
        });
    } else {
      res.status(403).send('Unauthorized')
    }
  }

  checkAccessTokenMiddleware(req, res, next) {
    let token = req.headers.authorization;
    token = token.substring(7);
    if (token) {
      firebase.functions.getUserByAccessToken(token)
        .then((user) => {
          req.custom = {
            'userId': user.id,
          };
          next()
        }).catch(() => {
          res.status(403).send('Unauthorized')
        });
    } else {
      res.status(403).send('Unauthorized')
    }
  }

  async link(req, res) {

    // Here, you should validate the user account.
    // In this sample, we do not do that.
    const code = decodeURIComponent(req.body.code)
    const authToken = decodeURIComponent(req.body.authToken)
    const responseurl = decodeURIComponent(req.body.responseurl)

    let uid = await firebase.functions.currentFirebaseUID(authToken);

    firebase.functions.updateUser(uid, undefined, undefined, undefined, code);

    return res.redirect(responseurl)
  }

  async auth(req, res) {

    let code = uuid.v4().replace(/-/g, '');

    const responseurl = util.format('%s?code=%s&state=%s',
      decodeURIComponent(req.query.redirect_uri), code,
      req.query.state);

    return res.redirect(`/login?responseurl=${encodeURIComponent(responseurl)}&code=${code}`)
  }

  async token(req, res) {

    const grantType = req.query.grant_type || req.body.grant_type
    const secondsInDay = 86400 // 60 * 60 * 24
    const HTTP_STATUS_OK = 200

    let obj
    if (grantType === 'authorization_code') {

      const authCode = req.query.code || req.body.code

      let user = await firebase.functions.getUserByAuthorizationCode(authCode);

      obj = {
        token_type: 'Bearer',
        access_token: uuid.v4().replace(/-/g, ''),
        refresh_token: uuid.v4().replace(/-/g, ''),
        expires_in: secondsInDay,
      }

      firebase.functions.updateUser(user.id, '', obj.access_token, obj.refresh_token, '');

    } else if (grantType === 'refresh_token') {

      const refresh_token = req.query.refresh_token ? req.query.refresh_token : req.body.refresh_token

      let user = await firebase.functions.getUserByRefreshToken(refresh_token);

      obj = {
        token_type: 'Bearer',
        access_token: uuid.v4().replace(/-/g, ''),
        expires_in: secondsInDay,
      }

      firebase.functions.updateUser(user.id, '', obj.access_token, refresh_token, '');
    } else {
      res.status(400).json({ "error": "invalid_grant" });
    }

    res.status(HTTP_STATUS_OK).json(obj)
  }

}

const authObject = new AuthClass();

module.exports = {
  login_get: authObject.login_get,
  login_post: authObject.login_post,
  checkAuthMiddleware: authObject.checkAuthMiddleware,
  checkAccessTokenMiddleware: authObject.checkAccessTokenMiddleware,
  link: authObject.link,
  auth: authObject.auth,
  token: authObject.token,
}