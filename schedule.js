const schedule = require('node-schedule');
const firebase = require('./firebase');

console.log('Init Schedule!');

// Run every 30 seconds
schedule.scheduleJob('*/30 * * * * *', async function (fireDate) {
    console.log('Checking devices online state', fireDate);
    let users = await firebase.functions.getUsers();
    users.forEach(async function (userId) {
        let devices = await firebase.functions.getDevices(userId);
        devices.forEach(async function (device) {
            let deviceId = device.id;

            let newStates = {
                'online': false,
                'needOnlineCheck': true,
            };

            firebase.functions.updateDeviceInRealtimeDatabase(userId, deviceId, newStates);

        });

    });
});

// Run every 2 minutes
schedule.scheduleJob('*/2 * * * *', async function (fireDate) {
    console.log('Refreshing child_changed listeners', fireDate);
    let users = await firebase.functions.getUsers();
    users.forEach(async function (userId) {
        let devices = await firebase.functions.getDevices(userId);
        devices.forEach(async function (device) {
            let deviceId = device.id;

            console.log(`Refreshed child_changed listener for uid:${userId} did:${deviceId}`, fireDate);

            firebase.db_realtime.ref('devices').child(userId).child(deviceId).off("child_changed");

            firebase.db_realtime.ref('devices').child(userId).child(deviceId).on("child_changed", function (snapshot) {
                if (snapshot) {
                  let statesToUpdate = {};
            
                  if (snapshot.key) {
                    statesToUpdate[snapshot.key] = snapshot.val();
                  }
            
                  firebase.functions.updateDevice(userId, deviceId, undefined, undefined, statesToUpdate, undefined, undefined, undefined, false);
                  firebase.functions.reportState(userId, deviceId, statesToUpdate);
                }
              });
          
        });

    });
});
